#  Valve Visaid

The visual aid is a simple app to help customers understand their valve device. The app was designed with engaging UI interactions to direct users through the graphics and copy in a way that they could efficiently grasp the information. My role was creating the UI using XCode and Swift to copy the designed UI as closely as possible. This project particularly improved my knowledge of constraints in creating a layout that could adapt to different sized devices. 

Created at Iperdesign by Tom Ignarri and Armaan Bhasin.

![1](images/vv1.png)

![2](images/vv2.png)

![3](images/vv3.png)

![5](images/vv5.png)




