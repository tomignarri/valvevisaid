//
//  SafetyInfoViewController.swift
//
//  Created by Eligio Sgaramella on 2/7/19.
//  Copyright © 2019 iperdesign. All rights reserved.
//

import UIKit

class SafetyInfoViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // Return to VC.
    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
