//
//  ViewController.swift
//
//  Created by Eligio Sgaramella on 2/1/19.
//  Copyright © 2019 iperdesign. All rights reserved.
//
//new update



import UIKit
import AVFoundation

class ViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var knownDangerContent: UIImageView!
    @IBOutlet weak var DummyTornadoBackground: UIImageView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var knownDangerHolder: UIView!
    @IBOutlet weak var TestLongGestureRecognizerView: KDCircularProgress!
    @IBOutlet weak var LongGestureRecognizer: UILongPressGestureRecognizer!
    @IBOutlet weak var unknownDangerHolder: UIView!
    @IBOutlet weak var topTextHolder: UIView!
    @IBOutlet weak var BrandLabel: UILabel!
    @IBOutlet weak var pressAndHoldLabel: UILabel!
    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var ISILabel: UILabel!
    @IBOutlet weak var ISIMenuButtonImage: UIImageView!
    
    @IBOutlet weak var knownDangerButton: UIView!
    @IBOutlet weak var unknownDangerButton: UIView!
    
    @IBOutlet var unknownDangerLabels: Array<UIView>!
    @IBOutlet var unknownDangerLabelImages: Array<UIView>!
    @IBOutlet var unknownDangerLabelImages2: Array<UIImageView>!
    var unknownDangerLabelToggle = [false, false, false, false, false, false, false, false, false, false]
    
    @IBOutlet weak var pageLabel1: UILabel!
    @IBOutlet weak var pageLabel2: UILabel!
    @IBOutlet weak var pageLabel3: UILabel!
    
    @IBOutlet weak var segueButton: UIButton!
    @IBOutlet weak var chartSegueView: UIView!
    
    var timer = Timer()
    
    var knownDangershown = false
    var unknownDangershown = false
    var topTextdefault = true
    
    var time = 0.0
    
    var pageDestination = 0
    
    var player: AVQueuePlayer?
    var playerLayer : AVPlayerLayer?
    var playerLooper: AVPlayerLooper?
    //var playerItem: AVPlayerItem?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = URL(fileURLWithPath:Bundle.main.path(forResource: "Tornado Background Comp 1_1", ofType: "mp4")!)
        let asset = AVAsset(url: url)
        
        let assetKeys = [
            "playable",
            "hasProtectedContent"
        ]
        
        let playerItem = AVPlayerItem(asset: asset,automaticallyLoadedAssetKeys: assetKeys)
        player = AVQueuePlayer(items: [playerItem])
        
        playerLooper = AVPlayerLooper(player: player!, templateItem: playerItem)
        
        playerLayer = AVPlayerLayer(player: player)
        playerLayer!.frame.size = self.view.bounds.size
        playerLayer!.videoGravity = AVLayerVideoGravity.resizeAspectFill
        self.view.layer.addSublayer(playerLayer!)
        playerLayer?.player = player
        player?.play()
        
        self.ISILabel.alpha = CGFloat(0.4)
        self.ISIMenuButtonImage.alpha = CGFloat(0.4)
        
        // Intialize that labels are hidden for Unknown Danger Popup.
        for labels in unknownDangerLabels {
            labels.isHidden = true
        }
        for images in unknownDangerLabelImages {
            images.isHidden = true
        }
        for i in 0...9 {
            unknownDangerLabelImages2[i].alpha = CGFloat(0.4)
        }
        
        menuView.isHidden = true
        
        self.knownDangerButton.alpha = CGFloat(0.4)
        self.unknownDangerButton.alpha = CGFloat(0.4)
 
        // Text for the BRAND REDACTED blue label that uses .baselineOffset to create a superscript.
        let bigFont = UIFont.systemFont(ofSize: 18)
        let smallFont = UIFont.systemFont(ofSize: 8)
        let title = NSMutableAttributedString(string: "BRAND REDACTED", attributes: [.font: bigFont])
        title.append(NSMutableAttributedString(string: "TM", attributes: [.font: smallFont, .baselineOffset: 6]))
        title.append(NSMutableAttributedString(string: " Plus", attributes: [.font: bigFont]))
        BrandLabel.textColor = UIColor.white
        BrandLabel.attributedText = title
        
        scrollView.delegate = self
        
        pageLabel1.isHidden = false
        pageLabel2.isHidden = true
        pageLabel3.isHidden = true
        
        pageLabel1.textColor = UIColor.red
        pageLabel2.textColor = UIColor.black
        pageLabel3.textColor = UIColor.black
        
        chartSegueView.isHidden = true
        
    }
    
    /*
    override func viewDidLayoutSubviews() {
        pageControl.subviews.forEach {
            $0.transform = CGAffineTransform(scaleX: 0.4, y: 2)
        }
    }
 */
    
     //move distance = (bottom edge of transparent black view y - height of holder) (598) + (button height) (67)
    @IBAction func knownDangerAnimation(_ sender: Any) {
        if (!unknownDangershown && !knownDangershown)
        {
            UIView.animate(withDuration: 1, animations:{
                self.knownDangerHolder.transform = self.knownDangerHolder.transform.translatedBy(x: 0, y: -self.view.frame.height)
                self.knownDangerButton.alpha = CGFloat(1.0)
                self.topTextHolder.transform = self.topTextHolder.transform.translatedBy(x: -1 * self.view.frame.width, y: 0)
                self.chartSegueView.transform = self.chartSegueView.transform.translatedBy(x: -1 * self.view.frame.width, y: 0)
                self.DummyTornadoBackground.alpha = CGFloat(0.6)
            })
            knownDangershown = true;
            topTextdefault = false;
        }
        else if(!unknownDangershown)
        {
            UIView.animate(withDuration: 1, animations:{
                self.knownDangerHolder.transform = self.knownDangerHolder.transform.translatedBy(x: 0, y: self.view.frame.height)
                self.knownDangerButton.alpha = CGFloat(0.4)
                self.topTextHolder.transform = self.topTextHolder.transform.translatedBy(x: self.view.frame.width, y: 0)
                self.chartSegueView.transform = self.chartSegueView.transform.translatedBy(x: self.view.frame.width, y: 0)
                self.DummyTornadoBackground.alpha = CGFloat(1.0)
            })
            knownDangershown = false;
            topTextdefault = true;
        }
        else if(unknownDangershown){
            UIView.animate(withDuration: 1, animations:{
                self.unknownDangerHolder.transform = self.unknownDangerHolder.transform.translatedBy(x: 0, y: self.view.frame.height)
                self.knownDangerButton.alpha = CGFloat(1.0)
                self.knownDangerHolder.transform = self.knownDangerHolder.transform.translatedBy(x: 0, y: -self.view.frame.height)
                self.unknownDangerButton.alpha = CGFloat(0.4)
            })
            unknownDangershown = false;
            knownDangershown = true;
        }
        print("known danger   ",knownDangershown)
        
    }
    
    //move distance = (bottom edge of transparent black view y - height of holder) (598) + (button height) (67)
    @IBAction func unknownDangerAnimation(_ sender: Any) {
        if (!unknownDangershown && !knownDangershown)
        {
            UIView.animate(withDuration: 1, animations:{
                self.unknownDangerHolder.transform = self.unknownDangerHolder.transform.translatedBy(x: 0, y: -self.view.frame.height)
                self.unknownDangerButton.alpha = CGFloat(1.0)
                self.topTextHolder.transform = self.topTextHolder.transform.translatedBy(x: -1 * self.view.frame.width, y: 0)
                self.chartSegueView.transform = self.chartSegueView.transform.translatedBy(x: -1 * self.view.frame.width, y: 0)
                self.DummyTornadoBackground.alpha = CGFloat(0.6)
            })
            unknownDangershown = true;
            topTextdefault = false;
        }
        else if(!knownDangershown)
        {
            UIView.animate(withDuration: 1, animations:{
                self.unknownDangerHolder.transform = self.unknownDangerHolder.transform.translatedBy(x: 0, y: self.view.frame.height)
                self.unknownDangerButton.alpha = CGFloat(0.4)
                self.topTextHolder.transform = self.topTextHolder.transform.translatedBy(x: self.view.frame.width, y: 0)
                self.chartSegueView.transform = self.chartSegueView.transform.translatedBy(x: self.view.frame.width, y: 0)
                self.DummyTornadoBackground.alpha = CGFloat(1.0)
            })
            unknownDangershown = false;
            topTextdefault = true;
        }
        else if(knownDangershown){
            UIView.animate(withDuration: 1, animations:{
                self.knownDangerHolder.transform = self.knownDangerHolder.transform.translatedBy(x: 0, y: self.view.frame.height)
                self.unknownDangerButton.alpha = CGFloat(1.0)
                self.unknownDangerHolder.transform = self.unknownDangerHolder.transform.translatedBy(x: 0, y: -self.view.frame.height)
                self.knownDangerButton.alpha = CGFloat(0.4)
            })
            knownDangershown = false;
            unknownDangershown = true;
        }
        print("unknown danger   ",unknownDangershown)
    }
    
    @objc func fireTimer() {
        time += 0.1
        TestLongGestureRecognizerView.animate(fromAngle: TestLongGestureRecognizerView.angle, toAngle: (time*360), duration: 0.1){ completed in
            if completed {
                print("animation stopped, completed")
            } else {
                print("animation stopped, was interrupted")
            }
        }
        if (time >= 1)
        {
            LongGestureRecognizer.isEnabled = false
            menuView.isHidden = false
            timer.invalidate();
        }
    }
    
    @IBAction func LongPressTest(_ sender: UILongPressGestureRecognizer)
    {
        print("is it doing things?")
        if (sender.state == .began)
        {
            print("began")
            timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(fireTimer), userInfo: nil, repeats: true)
        }
        else if (sender.state == .changed)
        {
            print("changed")
        }
        else if (sender.state == .ended)
        {
            timer.invalidate();
            time = 0;
            print("ended")
        }
        else if (sender.state == .possible)
        {
            print("possible")
        }
        else
        {
            print("something else")
            chartSegueView.isHidden = false
            TestLongGestureRecognizerView.isHidden = true
            pressAndHoldLabel.isHidden = true
        }
    }
    
    // Pressing a button contained in this action will show the labels on images in the Unknown Danger Popup
    @IBAction func unknownDangerShowLabels(_ sender: Any) {
        guard let button = sender as? UIButton else {
            return
        }
        if(!unknownDangerLabelToggle[button.tag]){
            //reset labels
            for labels in unknownDangerLabels {
                labels.isHidden = true
            }
            for images in unknownDangerLabelImages {
                images.isHidden = true
            }
            for i in 0...9 {
                unknownDangerLabelImages2[i].alpha = CGFloat(0.4)
            }
            var unknownDangerLabelToggle = [false, false, false, false, false, false, false, false, false, false]
            
            unknownDangerLabels[button.tag].isHidden = false
            unknownDangerLabelImages[button.tag].isHidden = false
            unknownDangerLabelImages2[button.tag].alpha = CGFloat(1.0)
            unknownDangerLabelToggle[button.tag] = !unknownDangerLabelToggle[button.tag]
        } else {
            unknownDangerLabels[button.tag].isHidden = true
            unknownDangerLabelImages[button.tag].isHidden = true
            unknownDangerLabelImages2[button.tag].alpha = CGFloat(0.4)
            unknownDangerLabelToggle[button.tag] = !unknownDangerLabelToggle[button.tag]
        }
    }
  
}

