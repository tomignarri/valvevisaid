
import UIKit

protocol Looper {
    
    init(videoURL: URL, loopCount: Int)
    
    func start(in layer: CALayer)
    
    func stop()
}
