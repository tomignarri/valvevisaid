import UIKit

class ViewController2: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var smallMagChart: UIButton!
    @IBOutlet weak var popUpMagChart: UIView!
    @IBOutlet weak var ContentView: UIView!
    @IBOutlet weak var smallOrderingInfo: UIButton!
    @IBOutlet weak var popUpOrderingInfo: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // Return to VC.
    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // Button action to enlarge chart.
    @IBAction func popUpChart(_ sender: UIButton) {
        smallMagChart.isHidden = true
        ContentView.alpha = 0.20
        scrollView.isUserInteractionEnabled = false
        popUpMagChart.isHidden = false
    }
    
    // Action to remove pop-up chart.
    @IBAction func unPopChart(_ sender: UIButton) {
        smallMagChart.isHidden = false
        ContentView.alpha = 1.0
        scrollView.isUserInteractionEnabled = true
        popUpMagChart.isHidden = true
    }
    
    // Button action to enlarge chart.
    @IBAction func popUpOrderingInfo(_ sender: UIButton) {
        smallOrderingInfo.isHidden = true
        ContentView.alpha = 0.20
        scrollView.isUserInteractionEnabled = false
        popUpOrderingInfo.isHidden = false
    }
    
    // Action to remove pop-up chart.
    @IBAction func upPopOrderingInfo(_ sender: UIButton) {
        smallOrderingInfo.isHidden = false
        ContentView.alpha = 1.0
        scrollView.isUserInteractionEnabled = true
        popUpOrderingInfo.isHidden = true
    }
    
}

